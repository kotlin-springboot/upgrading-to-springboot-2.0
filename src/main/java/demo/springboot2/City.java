package demo.springboot2;

import java.util.Objects;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class City {

  @Id
  private String id;
  private String name;
  private String country;

  public String getId() {
    return id;
  }

  public void setId(final String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(final String country) {
    this.country = country;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof City)) {
      return false;
    }
    final City city = (City) o;
    return Objects.equals(getId(), city.getId()) &&
        Objects.equals(getName(), city.getName()) &&
        Objects.equals(getCountry(), city.getCountry());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId(), getName(), getCountry());
  }
}
