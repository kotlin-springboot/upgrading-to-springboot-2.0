package demo.springboot2;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class CityController {

  private CityRepository repository;

  public CityController(CityRepository repository) {
    this.repository = repository;
  }

  @RequestMapping(path = "/cities")
  public Flux<City> all() {
    return this.repository.findAll().filter(CityController::isInUSA);
  }

  @RequestMapping(path = "/city/{name}")
  public Mono<City> byName(@PathVariable final String name) {
    return this.repository.findByNameIgnoringCase(name);
  }

  private static boolean isInUSA(final City city) {
    return city.getCountry().equals("USA");
  }
}
