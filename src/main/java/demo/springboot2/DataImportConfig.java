package demo.springboot2;

import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.bind.Bindable;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.boot.context.properties.source.MapConfigurationPropertySource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.mongodb.core.MongoOperations;

import de.flapdoodle.embed.mongo.Command;
import de.flapdoodle.embed.mongo.config.DownloadConfigBuilder;
import de.flapdoodle.embed.mongo.config.ExtractedArtifactStoreBuilder;
import de.flapdoodle.embed.mongo.config.RuntimeConfigBuilder;
import de.flapdoodle.embed.process.config.IRuntimeConfig;
import de.flapdoodle.embed.process.config.store.HttpProxyFactory;

@Configuration
public class DataImportConfig {

  @Bean
  CommandLineRunner init(MongoOperations mongo) {
    return (final String... args) -> {
      mongo.dropCollection(City.class);
      mongo.createCollection(City.class);
      getCities().forEach(mongo::save);
    };
  }

  private List<City> getCities() {
    final Properties yaml = loadCities();
    return new Binder(new MapConfigurationPropertySource(yaml))
      .bind("cities", Bindable.listOf(City.class)).get();

  }

  private Properties loadCities() {
    final YamlPropertiesFactoryBean properties = new YamlPropertiesFactoryBean();
    properties.setResources(new ClassPathResource("cities.yml"));
    return properties.getObject();
  }

  @Bean
  IRuntimeConfig embedMongoRuntimeConfigWithHttpProxy(
      @Value("${http-proxy-host}") final String httpHost,
      @Value("${http-proxy-port}") final int httpPort) {
    final Command command = Command.MongoD;
    return new RuntimeConfigBuilder()
        .defaults(command)
        .artifactStore(new ExtractedArtifactStoreBuilder()
            .defaults(command)
            .download(new DownloadConfigBuilder()
                .defaultsForCommand(command)
                .proxyFactory(new HttpProxyFactory(httpHost, httpPort))
                .build())
            .build())
        .build();
  }

}
