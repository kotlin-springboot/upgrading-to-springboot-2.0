# Upgrading to Spring Boot 2.0

[Webinar: Upgrading to Spring Boot 2.0](https://www.youtube.com/watch?v=G_0gOEw1Llc)


## Using embedded MongoDB with HTTP Proxy

 - Add the below dependency in `pom.xml`
  ```xml
    <dependency>
      <groupId>de.flapdoodle.embed</groupId>
      <artifactId>de.flapdoodle.embed.mongo</artifactId>
    </dependency>
  ```

 - Create a `bean` of type `de.flapdoodle.embed.process.config.IRuntimeConfig` in any of your config classes
  ```java
  @Configuration
  class Config {
    @Bean
    IRuntimeConfig embedMongoRuntimeConfigWithHttpProxy(
        @Value("${http-proxy-host}") final String httpHost,
        @Value("${http-proxy-port}") final int httpPort) {
      final Command command = Command.MongoD;
      return new RuntimeConfigBuilder()
          .defaults(command)
          .artifactStore(new ExtractedArtifactStoreBuilder()
              .defaults(command)
              .download(new DownloadConfigBuilder()
                  .defaultsForCommand(command)
                  .proxyFactory(new HttpProxyFactory(httpHost, httpPort))
                  .build())
              .build())
          .build();
    }
  }
  ```

 - `org.springframework.data.mongodb.core.MongoOperations` class will be available from Spring Context to be used in you code.
